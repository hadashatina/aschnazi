unit Dm_Message;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, Db, AtmAdvTable, AdvSearch;

Type
  TDllFuncStringNoParam=Function :ShortString;
  ELibNotFound = Class(Exception);

type
  TDMod_Message = class(TDataModule)
    Qry_Inter: TQuery;
    DataBase_InterFace: TDatabase;
    Qry_2: TQuery;
    Qry_3: TQuery;
    Tbl_AtmShib: TTable;
    Tbl_AtmAzmn: TTable;
    Rehev_Search: TAtmAdvSearch;
    Sql_Rehev: TQuery;
    Sql_Nehag: TQuery;
    Nehag_Search: TAtmAdvSearch;
    Tbl_Mname: TTable;
    sql_Lak: TQuery;
    Lak_Search: TAtmAdvSearch;

    procedure DMod_InterCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
//  procedure DMod_InterDestroy(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMod_Message: TDMod_Message;

  DBAliasName,
  DBUserName,
  DBPassword,
  DbIniFile : String;

  WinDir_Temp : string;    {Temp Windows ����� �}
  WinDirTmp : PChar;  {Windows ����� ����� ����� �}

  Function  RunDllStringFuncNoParam(DllName,DllProc :String):ShortString;

implementation

{$R *.DFM}

//----------------------------------------------------------------------
Procedure TDMod_Message.DMod_InterCreate(Sender: TObject);
begin
  if DBAliasName <>'' Then
     Begin
       Database_InterFace.Close;
       Database_InterFace.AliasName:=DBAliasName;
       Database_InterFace.Params.Values ['USERNAME']:= DBUserName;
       Database_InterFace.Params.Values ['PASSWORD']:= DBPassword;
//          if NetDir<>'' Then
//             Database_InterFace.Session.NetFileDir:=NetDir;
       Database_InterFace.Open;
     End;
end;
//----------------------------------------------------------------------
procedure TDMod_Message.DataModuleDestroy(Sender: TObject);
begin
    Try
      If Database_InterFace.Connected
       Then Database_InterFace.Connected:=False;
    except
      ON Exception do
    End;
end;

//----------------------------------------------------------------------
Function RunDllStringFuncNoParam(DllName,DllProc :String):ShortString;
Var
   DllHandle :THandle;
   ProcNoParam:TDllFuncStringNoParam;
   DN,DP:PChar;
Begin
     DN:=StrAlloc(Length(DllName)+1);
     DP:=StrAlloc(Length(DllProc)+1);
     Try
         StrPCopy(DN,DllName);
         StrPCopy(DP,DllProc);
         DllHandle:=LoadLibrary(DN);
         if DllHandle = 0 then
              Raise ELibNotFound.Create(DllName+' Not Found');
          @ProcNoParam:=GetProcAddress(DllHandle,DP);
          if @ProcNoParam <>  nil  then
             Result:=ProcNoParam;
          FreeLibrary(DllHandle);
          DllHandle:=0;
     Finally
            StrDispose(DN);
            StrDispose(DP);
     End;
End;

end.
