unit Tenuot_Carcom;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, FileCtrl, ExtCtrls, Mask, ToolEdit,
  Db, Grids, DBGrids, DBCtrls, AtmComp,DbTables ;

type
  TF_Tenuot_Carcom = class(TForm)
    BitBtn1: TBitBtn;
    Btn_Klita: TBitBtn;
    Klita_Dir: TFilenameEdit;
    Label1: TLabel;
    Label2: TLabel;
    Edt_Rehev: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Edt_Nehag: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Edt_Shana: TEdit;
    Label5: TLabel;
    Edt_Month: TEdit;
    Edt_shem_nehag: TEdit;
    Bbn_SaveIni: TBitBtn;
    BitBtn2: TBitBtn;
    Edt_Lak: TEdit;
    Label6: TLabel;
    Edt_Shem_lak: TEdit;
    SpeedButton3: TSpeedButton;
    procedure Btn_KlitaClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);

    Procedure Bedikat_Netunim;
    procedure SpeedButton2Click(Sender: TObject);
    procedure Edt_NehagChange(Sender: TObject);
    procedure FormShow(Sender: TObject);

    Procedure WriteRec;    
    Procedure WriteRec1;
    Procedure ReadIniFile;
    Procedure Bbn_SaveIniClick(Sender: TObject);
    Procedure ErrorLine(Mis_Shura,Sug_Takala,Mis_Teuda,Nehag,Rehev : String);
    Procedure BitBtn2Click(Sender: TObject);
    Procedure SpeedButton3Click(Sender: TObject);

    Procedure FindMhola;
    Procedure PutMerakezDataInField(MerakezName:String;TheField :TField);
    Procedure Edt_LakChange(Sender: TObject);
    Function  DosToWin(WinString : String) : String;
    Function  TranslLetterDosToWin( Ch:char):char;
    Function  LeftToRight(InStr:string):string;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Call_Tenuot_Carcom(LDbAliasName,LDbUserName,LDbPassword,LIniFile:Pchar);
var
  F_Tenuot_Carcom: TF_Tenuot_Carcom;

  PrintText :System.Text;   {���� ������}

  Sw_Stop,Found : boolean; // ����� ������
Type
    FileZ = Record
//    Shura : Char;
    end;
Var
    F: File Of byte; // Filez;

    Sw_Takin : Boolean;

    Kod_Nehag,Mis_Rishui,
    File_Name,Kod_Lak,Teuda              : String;
    TotalPrice                           : Real;

//������� ������ ������ ����� ��� �� �����
Function ConvertToRishuy(InStr :String) :String;
Function DelLeadingZeroFromRishuy (SrcStr:String):String;
function DosToWin(InStr:string):string;
function LeftToRight(InStr:string):string;

// ���� ���� ������� ������ �"� �� �����
Function CountFieldsInSepString(TheString:String;TheSeparator :Char):LongInt;

//������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                    FieldNum :LongInt): String;

implementation

Uses F_Mesage, IniFiles, Dm_Message;
{$R *.DFM}

Var
  IniFile: TIniFile;
  Mitan : Array[1..2,1..100] Of String; // 500
  Total_Records ,Mis_Shura,
  Sug_Rec,Mone1                    : integer;
  Amount                           : Real;
  PrintStr ,HStr                   : String;
  Rehev,Tuda,Taarich_Temp,Lako,Homer,LoadType,
  Taarich_Bitzua,Maslul,Remark,Pratim,
  ShemLak,Nehag,ShemNehag,ShemMaslul,FromPlace,Toplace,
  Asmacta,Neto                     : ShortString;
//  PrintText :System.Text;   {���� ������}
  CarcomFile :TextFile;      {���� �����}
  LastAzmnValue  : Integer; // ����� ��' ����� �����
  FirstAzmnValue : Integer; // ��' ����� �����
  PreAzmn        : Integer;

//----------------------------------------------------------------------
Procedure TF_Tenuot_Carcom.Btn_KlitaClick(Sender: TObject);
Var

  F_Mesage : TF_Message;    {���� ����� ���� ������}

//Var_Sug_rikuz : String;
begin
  if MessageDlg('? �� �� ������ ���� ������� ��� ������', mtConfirmation,[mbYes, mbNo], 0) = mrNo then
     exit;

  Bedikat_Netunim;   //    ����� ������

  if Not Sw_takin then
     exit;
//--------------------------
  Sw_Stop := false;
//-----------------
//DMod_Message.Tbl_AtmAzmn.Open;
//DMod_Message.Tbl_AtmShib.Open;
//-----------------  ��' ������� �����

  AssignFile (F, (Klita_Dir.Text));
  Reset (F);
  Total_Records := Trunc (FileSize (F)/ 190)+1;
  CloseFile (F);
//---------------------------------
  AssignFile (CarcomFile, (Klita_Dir.Text));
  Reset (CarcomFile);
  AssignFile (PrintText, (ExtractFilePath(Application.ExeName) + 'Shagui.Txt'));
  Rewrite (PrintText);

  F_Mesage := TF_Message.Create (Self);
  F_Mesage.Show;
  F_Mesage.Caption := '����� ���� ������ ���';

  With DMod_Message.Qry_Inter do
  Begin
    Close;
    Sql.Clear;
    Sql.Add('Select *');
    Sql.Add('from kodtavla');
    Sql.Add('where Sug_tavla = 500');
    Open;
    First;
    Mone1:=0;
    While Not Eof Do
    Begin
      Inc(Mone1);
      Mitan[1,Mone1]:=FieldByName('Kod_tavla').Asstring;
      Mitan[2,Mone1]:=FieldByName('Teur_tavla').Asstring;
      Next;
    End; // While Not Eof
  End; // With DMod_Message.Qry_Inter
  Mis_Shura := 0;
//----------------------- ��' ����� ����� ������
  With DMod_Message.Qry_Inter do
     Begin
       Close;
       Sql.Clear;
       Sql.Add ('Select Max(AzmnNo) As MaxNum From AtmAzmn');
       Open;
     End;
  LastAzmnValue := DMod_Message.Qry_Inter.FieldByName('MaxNum').AsInteger;
  FirstAzmnValue:=LastAzmnValue+1;
  Lako:=Edt_lak.Text;
  ShemLak:=Edt_Shem_lak.Text;
  F_Mesage.Total_Records.Caption := IntToStr(Total_Records);
  Application.ProcessMessages;
  Found:=False;

  //-------------------------------
  While (Not Eof(CarcomFile)) AND
        (Not sw_Stop)  do
    Begin
      Inc (Mis_Shura);
      F_Mesage.Mis_Shura.Caption := IntToStr(Mis_Shura);
      F_Mesage.AtmGauge1.Progress:=Trunc ((Mis_Shura  / Total_RecordS )*100);

      Readln (CarcomFile,HStr);
      PrintStr := '';           {���� ���� �������}
//---------------------- ���� �������
      Asmacta:=Trim(Copy(HStr,4,10));     //  ������

      With DMod_Message.Qry_2 do // ����� ��� ������ �����
      Begin
        Close;
        Sql.Clear;
        Sql.add('Select ShibAsmcta');
        Sql.add('From Atmshib');
        Sql.add('Where ShibAsmcta='+Asmacta );
        Sql.Add('And LakNo1='+Lako);
        Sql.Add('And ShibAzmnNo<'+InttoStr(FirstAzmnValue));
        Sql.SaveToFile('c:a.sql');
        Open;
      End; // With DMod_Message.Qry_2 do
      If DMod_Message.Qry_2.Eof Then
      Begin
        Found:=True;
        Sug_Rec:=StrToInt(Copy(HStr,1,3));
        Case Sug_Rec Of
        1 : Begin
            Inc(LastAzmnValue);
            Tuda :='0'; //   �����
//          Rehev:='%-'+Copy(HStr,88,3)+'-%';   // ��'-���
            Rehev:=Copy(HStr,83,9);             // ��'-���
            Neto:='1';                          // ����
            Asmacta:=Trim(Copy(HStr,4,10));     //  ������
            Taarich_Bitzua:=Copy(HStr,17,3)+Copy(HStr,14,2)+
                            Copy(HStr,19,3);    //����� ������
            LoadType:=LeftToRight(DosToWin(Trim(Copy(HStr,322,15))));// ��� ����
            Maslul:='0';                        // �����
            ShemNehag:=LeftToRight(DosToWin(Trim(Copy(HStr,42,30))));  // ���
            FromPlace:=LeftToRight(DosToWin(Trim(Copy(HStr,132,30))));  // ����
            ToPlace:=LeftToRight(DosToWin(Trim(Copy(HStr,182,30))));  // ���
            Pratim:=LeftToRight(DosToWin(Trim(Copy(HStr,212,50))));  // �����
            ShemMaslul:=ToPlace+' '+FromPlace;
            ShemMaslul:=Copy(ShemMaslul,1,40);
//------------------------------------
            With DMod_Message.Qry_2 do
            Begin
                Try
                Close;
                Sql.Clear;
                Sql.Add ('Select Shem_Nehag,R.Kod_Nehag,R.Mis_Rishui From Rehev R,Nehag N');
//              Sql.Add ('Where R.Mis_Rishui Like ('+QuotedStr(Rehev)+')');
                Sql.Add ('Where R.Mis_Rishui ='+ QuotedStr(Rehev));
                Sql.Add ('And R.Kod_Nehag=N.Kod_Nehag');
                Open;
                If Eof And Bof Then
                Begin
                  Nehag:=Edt_Nehag.Text;
                  Rehev:=Edt_Rehev.Text;
//                ShemNehag:=LeftToRight(DosToWin(ShemNehag));
                End
                Else
                Begin
                  ShemNehag:=
                    DMod_Message.Qry_2.FieldByname('Shem_nehag').AsString;
                  Nehag:=DMod_Message.Qry_2.FieldByname('Kod_Nehag').AsString;
                  Rehev:=DMod_Message.Qry_2.FieldByname('Mis_Rishui').AsString;
                End;
               Except
                 ErrorLine(IntToStr(Mis_Shura),'��� �� ����',Asmacta,Nehag,Rehev);
               End;
            End;
           WriteRec;
          End; // Sug_Rec=1
      3,5 : Begin
            If Sug_Rec=3 Then
            Begin
              Remark:=Trim(Copy(HStr,64,20));
              Amount:=StrToFloat(Copy(HStr,84,10));
              Homer:=Copy(HStr,62,2);
              FindMhola;
              Writerec1;
            End;

            If Sug_Rec=5 Then
            Begin
              Remark:=LeftToRight(DosToWin(Trim(Copy(HStr,14,20))));
              Amount:=StrToFloat(Copy(HStr,44,10));
              Writerec1;
            End;
          End; // Sug_rec 2,3
      End; // Sug_rec
    End; // With DMod_Message.Qry_2 do // ����� ��� ������ �����
    End;  {end while}
//  End;
  CloseFile (CarcomFile);
  System.Close (PrintText);

  F_Mesage.Free;
  F_Mesage := nil;


//DMod_Message.Tbl_AtmAzmn.Close;
//DMod_Message.Tbl_AtmShib.Close;

  iF Sw_Stop then
     showmessage ('����� ������ ������ �"� ������')
  Else
    If Found Then
       Showmessage(' ���� ������ '+
         #13+#13+InttoStr(FirstAzmnValue)+'-'+InttoStr(LastAzmnValue))
    Else
       Showmessage('�� ����� ������ ������');
//   ('������ ����� ������');

//----------------------- ���� ��"� ������
//  if Frm_Shagui = Nil then
//     Application.CreateForm(TFrm_Shagui, Frm_Shagui);

//  Frm_Shagui.QuickReport.Preview;
//End;
//---------------------------------------------------------------------

End;
//--------------------------- ���� ��������

Procedure TF_Tenuot_Carcom.WriteRec1;
Begin
        With DMod_Message.Qry_3 do
        Begin
          Close;
          Sql.Clear;
          Sql.Add('Insert Into AtmShib');
          Sql.Add('(ShibAzmnNo,ShibAzmnDate,ShibKind,AzmnOk,');
          Sql.Add('CarNum,UpdateRecordDate,ShibTuda,');
          Sql.Add('ShibBeginTime,ShibEndTime,LakNo1,');
          Sql.Add('YeMida1,YeMida2,YeMida3,YeMida4,');
          Sql.Add('MaklidName,YehusMonth,YehusYear,');
          Sql.Add('DriverNo1,DriverName,');
          Sql.Add('Quntity1,Quntity2,Quntity3,Quntity4,');
          Sql.Add('Price1,Price2,Price3,Price4,');
          Sql.Add('PriceQuntity1,PriceQuntity2,PriceQuntity3,PriceQuntity4,');
          Sql.Add('SugMetan,MaslulCode1,Maslul1,');
          Sql.Add('HovalaKind,PriceKind,ShibTotalKm,LakNo2,LakNo3,');
          Sql.Add('PriceIsGlobal,Nefh,QuntMusa,TotalHour,Mishkal,');
          Sql.Add('DriverNo2,CarNo,LakCodeBizua,LakHesbonit,DrishNo,');
          Sql.Add('DriverCodeBizua,DriverZikuiNo,CarCodeBizua,CarZikuiNo,TrvlNo,');
          Sql.Add('StarMetanNo,GpsStatus,GpsNo,ShibCarKind,TnuaOk,');
          Sql.Add('HasbonitOk,FirstSpido,TudaKind,NegrrNo,ShibAsmcta,');
          Sql.Add('ShibFromPalce,ShibToPalce,ShibRem2,ShibRem1');
          Sql.Add(')');
          Sql.Add('Values (');
          Sql.Add(InttoStr(LastAzmnValue));
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(',1,1');
          Sql.Add(','+QuotedStr(Rehev));
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(','+Tuda);
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(','+Lako);
          Sql.Add(',1,1,1,1,1');
          Sql.Add(','+Edt_Month.Text);
          Sql.Add(','+Edt_Shana.Text);
          Sql.Add(','+Nehag);
          Sql.Add(','+QuotedStr(ShemNehag));
          Sql.Add(','+Floattostr(StrToFloat(Neto))); //���� �
          Sql.Add(','+Floattostr(StrToFloat(Neto))); //���� �
          Sql.Add(','+Floattostr(StrToFloat(Neto))); //���� �
          Sql.Add(','+Floattostr(StrToFloat(Neto))); //���� �
///       Sql.Add(',1,1,1');  David 29-1-2006
          Sql.Add(','+Floattostr(Amount));
          Sql.Add(','+Floattostr(Amount));
          Sql.Add(',0,0');
          Sql.Add(','+Floattostr(Amount));
          Sql.Add(','+Floattostr(Amount));
          Sql.Add(',0,0');
          Sql.Add(','+Homer+','+Maslul);
          Sql.Add(','+QuotedStr(ShemMaslul));
          Sql.Add(',1,0,0,0,0');
          Sql.Add(',0,0,0,0,0');
          Sql.Add(',0,0,Null,Null,Null');
          Sql.Add(',Null,Null,Null,Null,0');
          Sql.Add(',0,0,0,0,1');
          Sql.Add(',0,0,0,0,');
          Sql.Add(Asmacta+',');
          Sql.add(QuotedStr(FromPlace)+','+QuotedStr(Toplace)+','+QuotedStr(Remark));
          Sql.add(','+QuotedStr(Pratim));
          Sql.Add(')');
          Try
            DMod_Message.Qry_3.ExecSQL;
          Except
          On E:Exception do
          Begin
            Sql.SaveToFile('carcom1.sql');
            ShowMessage (E.Message);
            ErrorLine(IntToStr(Mis_Shura),'���� ������ ������',Asmacta,Nehag,Rehev);
          End;
          End; //Exception
        End;//With DMod_Message.Qry_3
End; //Writrec1

Procedure TF_Tenuot_Carcom.WriteRec;
Begin
        With DMod_Message.Qry_3 do
        Begin
          Close;
          Sql.Clear;
          Sql.Add('Insert Into AtmAzmn');
          Sql.Add('(AzmnNo,InvLakNo,InvName,AzmnDate,FromDate,ToDate,');
          Sql.Add('TotalPrice1,TotalPrice2,TotalPrice3,Quntity1,AzmnQuntity,');
          Sql.Add('AzmnType,Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,');
          Sql.Add('Saturday,MaklidName,Asmcta,Tuda,TotalKm,TotalZikuim,OldAzmnType,');
          Sql.Add('AzmnCarKind,AzmnSugMetan,DirshNo,FirstAzmnNo,AzmnMaslulCode1,');
          Sql.Add('AzmnMaslulCode2,AzmnMaslulCode3,AzmnMaslulCode4,AzmnCarNo,AzmnCarNum,');
          Sql.Add('AzmnDriverNo,CodeMhiron,Mitzar,Rshimon,Nefh,Damaged,HovalaKind');

          Sql.Add(')');
          Sql.Add('Values (');
          Sql.Add(InttoStr(LastAzmnValue));
          Sql.Add(','+Lako);
          Sql.Add(','+QuotedStr(Copy(ShemLak,1,20)));
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(','+QuotedStr(Taarich_Bitzua));
          Sql.Add(','+Floattostr(Amount));
          Sql.Add(','+Floattostr(Amount));
          Sql.Add(',0,'+Floattostr(StrToFloat(Neto)));
          Sql.Add(','+Floattostr(StrToFloat(Neto)));
          Sql.Add(',2,1,1,1,1,1,1,1,1,'+Asmacta+','+Tuda);
          Sql.Add(',0,0,0,0');
          Sql.Add(',0,0,0,'+Maslul);
          Sql.Add(',0,0,0,0,'+QuotedStr(Rehev));
          Sql.Add(','+Nehag);
          Sql.Add(',0,0,0,0,0,0'+Homer);
          Sql.Add(')');
          Sql.SaveToFile('C:\carcom.sql');
          Try
            DMod_Message.Qry_3.ExecSQL;
          Except
          On E:Exception do
          Begin
            ShowMessage (E.Message);
            ErrorLine(IntToStr(Mis_Shura),'���� ������ ������',Asmacta,Nehag,Rehev);
          End;
          End; //Exception
        End; //With DMod_Message.Qry_3
End;// writerec;

procedure TF_Tenuot_Carcom.SpeedButton1Click(Sender: TObject);
begin
  if Dmod_Message.Rehev_Search.Execute then
    begin
      Edt_Rehev.Text:= Dmod_Message.Rehev_Search.ReturnString;
    end; {end if}
end;
//---------------------------------------------------------------------
Procedure TF_Tenuot_Carcom.Bedikat_Netunim;
begin
  Sw_Takin := True;

  If (Edt_Shana.Text = '')  or
     (Edt_Month.Text = '') then
     begin
       ShowMessage ('��� ���� �/�� ��� ����');
       Sw_Takin := False;
       Exit;
     end;

  If Edt_Rehev.Text = '' then
     begin
       ShowMessage ('��� ��� ����');
       Sw_Takin := False;
       Exit;
     end;

  If Edt_Nehag.Text = '' then
     begin
       ShowMessage ('��� ��� ���');
       Sw_Takin := False;
       Exit;
     end;

  Klita_Dir.Text:=StringReplace(Klita_Dir.Text,'"','',[Rfreplaceall]);
  if not FileExists(Klita_Dir.Text) then
     Begin
       ShowMessage ('������� �� ����� �/�� ����� �� ����');
       Sw_Takin := False;
       Exit;
     end;

   With DMod_Message.Qry_3 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Mis_Rishui From Rehev');
         Sql.Add ('Where Mis_Rishui = '+''''+Edt_Rehev.Text+'''');
         Open;

         if FieldByName ('Mis_Rishui').AsString <> Trim(Edt_Rehev.Text) then
            begin
              ShowMessage ('��� �� ���� ������');
              Sw_Takin := False;
              Exit;
            end;
       end;

   With DMod_Message.Qry_2 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Kod_Nehag From Nehag');
         Sql.Add ('Where Kod_Nehag = '+Edt_Nehag.Text);
         Open;

         if FieldByName ('Kod_Nehag').AsString <> Edt_Nehag.Text then
            begin
              ShowMessage ('��� �� ���� ������');
              Sw_Takin := False;
              Exit;
            end;
       end;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Carcom.SpeedButton2Click(Sender: TObject);
begin
  if Dmod_Message.Nehag_Search.Execute then
    begin
      Edt_Nehag.Text:= Dmod_Message.Nehag_Search.ReturnString;
    end; {end if}
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Carcom.Edt_NehagChange(Sender: TObject);
begin
   With DMod_Message.Qry_2 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Kod_Nehag,Shem_Nehag From Nehag');
         Sql.Add ('Where Kod_Nehag = '+Edt_Nehag.Text);
         Open;

         if FieldByName ('Kod_Nehag').AsString = Edt_Nehag.Text then
            begin
              Edt_Shem_nehag.Text := FieldByName ('Shem_Nehag').AsString;
            end
         else
            Edt_Shem_nehag.Text := '';
       end;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Carcom.FormShow(Sender: TObject);
var
  Year, Month, Day : Word;
begin
  DecodeDate(Date, Year, Month, Day); {����� ���� ������}
  Edt_Month.Text := IntToStr(Month);
  Edt_Shana.Text := IntToStr(Year);

  ReadIniFile;
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Carcom.ReadIniFile;
begin
  IniFile := TIniFile.Create(DbIniFile);

  Edt_Nehag.Text := IniFile.ReadString ('Carcom', 'Kod_Nehag' ,'');
  Edt_Lak.Text   := IniFile.ReadString ('Carcom', 'Kod_Lak' ,'');
  Edt_rehev.Text := IniFile.ReadString ('Carcom', 'Mis_Rishui' ,'');
  Klita_Dir.Text := IniFile.ReadString ('Carcom', 'File_Name' ,'');

  IniFile.Free;

  Kod_Nehag := Edt_Nehag.Text;
  Kod_Lak   := Edt_Lak.Text;
  Mis_Rishui:= Edt_Rehev.text;
  File_Name := Klita_Dir.Text;
end;
//---------------------------------------------------------------------
procedure TF_Tenuot_Carcom.Bbn_SaveIniClick(Sender: TObject);
begin
  IniFile := TIniFile.Create(DbIniFile);

  IniFile.WriteString ('Carcom', 'Kod_Nehag' ,Edt_Nehag.Text);
  IniFile.WriteString ('Carcom', 'Mis_rishui',Edt_Rehev.Text);
  IniFile.WriteString ('Carcom', 'File_Name' ,Klita_Dir.Text);
  IniFile.WriteString ('Carcom', 'Kod_lak'   , Edt_Lak.Text);

  IniFile.Free;

  Kod_Nehag := Edt_Nehag.Text;
  Mis_Rishui:= Edt_Rehev.text;
  File_Name := Klita_Dir.text;
  Kod_lak   := Edt_Lak.Text
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Carcom.ErrorLine(Mis_Shura,Sug_Takala,Mis_Teuda,Nehag,Rehev : String);
begin
  Writeln (PrintText,
  Mis_Shura:5,'   ',
  Sug_Takala:25,'   ',
  Mis_Teuda:10,'   ',
  Nehag:10,'   ',
  Rehev:8,'   ');
end;
//----------------------------------------------------------------------
procedure TF_Tenuot_Carcom.BitBtn2Click(Sender: TObject);
begin
  Sw_Stop := True;
end;
//----------------------------------------------------------------------
Function ConvertToRishuy(InStr :String) :String;
Var
   StrH :String;
Begin
   StrH:= InStr;
   Case Length(StrH) of
        5:Result:=Copy(StrH,1,2)+'-'+Copy(StrH,3,3);
        6:Result:=Copy(StrH,1,3)+'-'+Copy(StrH,4,3);
        7:Result:=Copy(StrH,1,2)+'-'+Copy(StrH,3,3)+'-'+Copy(StrH,6,2);
   Else
       Result:=InStr;
   End;
end;

Procedure TF_Tenuot_Carcom.SpeedButton3Click(Sender: TObject);
Begin
  if Dmod_Message.Lak_Search.Execute then
    begin
      Edt_Lak.Text:= Dmod_Message.Lak_Search.ReturnString;
    end; {end if}

End;

Function DelLeadingZeroFromRishuy (SrcStr:String):String;
Var
  I:Integer;
  Tmp:String;

  Sw : Boolean;
begin
  Tmp:='';

  sw := False;
  For i:=1 To Length (SrcStr) Do
     if sw  then
        Tmp := Tmp+SrcStr[i]
     else
       begin
         if SrcStr[i] > '0' then
            begin
              Tmp := Tmp+SrcStr[i];
              sw := True;
            end
         else
            Tmp := Tmp+ ' ';
       end;
  Result := Tmp;
end;


function DosToWin(InStr:string):string;
var
  i : Integer;
begin
  for i := 1 To Length (InStr) Do
    if (Ord (InStr[i]) >= 128) And (Ord (InStr[i]) <=154 ) Then
       InStr[i] := Chr (Ord (InStr [i]) + 96)
    else
       InStr[i] := Chr (Ord (InStr [i]));
  DosToWin := InStr;
end;
//-----------------------------------------------------------------------
function LeftToRight(InStr:string):string;
var
   ix1,i:integer;
   HelpStr,Tmp,Tmp1:string;
begin
  HelpStr:='';
  Tmp := '';
  Tmp1:='';
  for ix1:= Length(InStr) downto 1 do
      HelpStr:=HelpStr+InStr[ix1];
  for ix1:= 1 to Length(HelpStr) Do
  begin
    if ((HelpStr[ix1] <'0') Or (HelpStr[ix1] >'9'))And (HelpStr[ix1]<>'.')And (HelpStr[ix1]<>',') Then
      begin
         For i:= Length (tmp1)downto 1  do
            Tmp:=Tmp+Tmp1[i];
         Tmp1:='';
         Tmp:=Tmp+HelpStr[ix1];
      end
    else
      Tmp1 := Tmp1+HelpStr[ix1]
  end;
  LeftToRight:=tmp;
end;

procedure TF_Tenuot_Carcom.PutMerakezDataInField(MerakezName:String;TheField :TField);
Var
   I,NumOfFields :LongInt;
   FieldName,StrH :String;
   FieldBuffer:String;
   Begin
     FieldName:='';
     FieldBuffer:='';
     if Trim(MerakezName)='' Then Exit;
     NumOfFields:=CountFieldsInSepString(MerakezName,',');
     Try
        For I:=1 To NumOfFields Do
        Begin
           FieldName:=GetFieldFromSeparateString(MerakezName,',',I);
           StrH:=Dmod_Message.Tbl_AtmShib.FieldByName(FieldName).AsString;
           if Length(StrH)> 10 Then
              StrH:=Copy(StrH,1,10);
           While Length(StrH)<10 Do
           Begin
                 if Dmod_Message.Tbl_AtmShib.FieldByName(FieldName) is TNumericField Then
                    StrH:='0'+StrH
                 Else
                    StrH:=StrH+' ';
           End;
           FieldBuffer:=FieldBuffer+StrH;

        End;
        if Dmod_Message.Tbl_AtmShib.State=dsBrowse Then
           Dmod_Message.Tbl_AtmShib.Edit;
        TheField.AsString:=FieldBuffer;
     Except On e:Exception Do
          ShowMessage('�� ���� ������ ����'+
                      #13+E.Message);
     End;
End;

Function CountFieldsInSepString(TheString:String;TheSeparator :Char):LongInt;
// ���� ���� ������� ������ �"� �� �����
Var
   I :LongInt;
   NumOfSep :LongInt;
Begin
     if TheString='' Then
     Begin
          Result:=0;
          Exit;
     End;
     NumOfSep:=0;
     For I:=1 To Length(TheString) Do
     Begin
         if TheString[i]=TheSeparator Then
            Inc(NumOfSep);
     End;
     Result:=NumOfSep+1;
End;

//         ������� ������� ��� ��� ������ ���� ������ �� ���� �����
Function GetFieldFromSeparateString(TheString :String;Separator :Char;
                                         FieldNum:LongInt): String;
Var
   StrH :String;
   Ix1,FieldCount,FieldPos :LongInt;

Begin
    if TheString='' Then
    Begin
        Result:='';
        Exit;
    End;
    if (FieldNum=1) Then
    Begin
        if TheString[1]=Separator Then
             Result:=''
        Else
        Begin
             StrH:=Copy(TheString,1,Pos(Separator,TheString)-1);
             if StrH='' Then StrH:=TheString;
             {EndOfString:=Copy(TheString,Pos(Separator,TheString)+1,Length(TheString)-Length(StrH)+1);}
             Result:=StrH;
        End;
    End
    Else {FieldNum>1}
    Begin
         FieldCount:=1;
         For Ix1:=1 To Length(TheString) Do
            if TheString[Ix1]=Separator Then
            Begin
                 FieldPos:=Ix1+1;
                 FieldCount:=FieldCount+1;
                 if FieldCount=FieldNum Then Break;
            End;
         if (FieldCount=FieldNum) And (FieldPos<=Length(TheString)) Then
         Begin
             StrH:=Copy(TheString,FieldPos,Length(TheString)-FieldPos+1);
             if Pos(Separator,StrH)>0 Then
                StrH:=Copy(StrH,1,Pos(Separator,StrH)-1);
             Result:=StrH;
         End
         Else
             Result:='';
    End;
End;

procedure Call_Tenuot_Carcom(LDbAliasName,LDbUserName,LDbPassword,LIniFile:Pchar);
begin
  DbAliasName := LDbAliasName;
  DbUserName := LDbUserName;
  DbPassword := LDbPassword;
  DbIniFile := LIniFile;

  If Dmod_Message = Nil Then
     Dmod_Message := TDmod_Message.Create (Nil);

  If F_Tenuot_Carcom = Nil then
     F_Tenuot_Carcom := TF_Tenuot_Carcom.Create (Nil);
  F_Tenuot_Carcom.ShowModal;
//----------------
  If Dmod_Message <> nil then
   begin
     Dmod_Message.Free;
     Dmod_Message := Nil;
   end;

  If F_Tenuot_Carcom <> nil then
   begin
     F_Tenuot_Carcom.Free;
     F_Tenuot_Carcom := Nil;
   end;

end;


procedure TF_Tenuot_Carcom.Edt_LakChange(Sender: TObject);
begin
   With DMod_Message.Qry_2 do
       begin
         Close;
         Sql.Clear;
         Sql.Add ('Select Kod_Lakoach,Shem_Lakoach From Lakoach');
         Sql.Add ('Where Kod_Lakoach = '+Edt_Lak.Text);
         Open;

         if FieldByName ('Kod_Lakoach').AsString = Edt_Lak.Text then
            begin
              Edt_Shem_Lak.Text := FieldByName ('Shem_Lakoach').AsString;
            end
         else
            Edt_Shem_Lak.Text := '';
       end;

end;

function TF_Tenuot_Carcom.LeftToRight(InStr:string):string;
 var
   ix1,i:integer;
   HelpStr,Tmp,Tmp1:string;
begin
  HelpStr:='';
  Tmp := '';
  Tmp1:='';
  for ix1:= Length(InStr) downto 1 do
     HelpStr:=HelpStr+InStr[ix1];
  for ix1:= 1 to Length(HelpStr) Do
  begin
      if ((HelpStr[ix1] <'0') Or (HelpStr[ix1] >'9'))And (HelpStr[ix1]<>'.') And (HelpStr[ix1]<>',') And (HelpStr[ix1]<>'-') Then
      begin
         For i:= Length (tmp1)downto 1  do
            Tmp:=Tmp+Tmp1[i];
         Tmp1:='';
         Tmp:=Tmp+HelpStr[ix1];
      end
      else
        Tmp1 := Tmp1+HelpStr[ix1]
  end;
  For i:= Length (tmp1)downto 1  do
     Tmp:=Tmp+Tmp1[i];

  LeftToRight:=tmp;
end;
function TF_Tenuot_Carcom.TranslLetterDosToWin( Ch:char):char;
begin
   case ord(Ch) of
     40,41,46: TranslLetterDosToWin:=chr(32);
     127..156: TranslLetterDosToWin:=chr(ord(ch)+96);
     else TranslLetterDosToWin:=ch;
   end;
end;

function TF_Tenuot_Carcom.DosToWin(WinString : String) : String;
var
  CountI : Integer;
begin
  for CountI:=1 to Length(WinString) do
    WinString[CountI]:=TranslLetterDosToWin(WinString[CountI]);
  Result := WinString;
end;

Procedure TF_Tenuot_Carcom.FindMhola;
var
  Count     : Integer;
  NewHomer  : String;
Begin
   NewHomer:='0';
   For Count:=1 to Mone1 Do
   Begin
     If (Pos(Homer,Mitan[2,Count])<>0)  And
       (Pos(LoadType,Mitan[2,Count])<>0) Then
       Begin
         NewHomer:=Mitan[1,Count];
         Break;
       End;
   End ;
   Homer:=NewHomer;

End;

End.

