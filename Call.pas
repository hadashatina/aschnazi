unit Call;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, ExtCtrls,DbTables, AtmComp;

type
  TFrm_CallDll = class(TForm)
    Button1: TButton;
    AtmHComboBox1: TAtmHComboBox;
    Label9: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AtmHComboBox1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_CallDll: TFrm_CallDll;
  DBAliasName1  : String;

  procedure Call_Tenuot_Carcom(DbAliasName,DbUserName,DbPassword,AIniFile: Pchar);far;external 'Elad2Atm.Dll';

implementation

Uses IniFiles;
Var
IniFile       : TIniFile;

{$R *.DFM}

//---------------------------------------------------------------------
procedure TFrm_CallDll.Button1Click(Sender: TObject);
Var
ShemIni  :  String;

Begin
  ShemIni:=ExtractFilePath(Application.ExeName)+'\AtmAzmn.Ini';
//Call_Tenuot_Carcom('Atm','Sa','','C:\AtmAzmn.Ini');
  Call_Tenuot_Carcom(Pchar(DBAliasName1),'Sa','',Pchar(ShemIni));
End;

Procedure TFrm_CallDll.FormShow(Sender: TObject);
Var
  MyStringList  :  TstringList;
  I             :  Integer;
Begin
  IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\AtmAzmn.Ini');
  DBAliasName1 := IniFile.ReadString ('Carcom', 'AliasName' ,'Atm');
  IniFile.Free;

  MyStringList := TStringList.Create;
  Try
  Session.GetAliasNames(MyStringList);
  for  I := 0  to  MyStringList.Count - 1  do
    AtmHComboBox1.Items.Add(MyStringList[I]);
  finally
    MyStringList.Free;
  End ;

 AtmHComboBox1.ItemIndex:=
    AtmHComboBox1.Items.IndexOf(DBAliasName1);

end;

procedure TFrm_CallDll.AtmHComboBox1Change(Sender: TObject);
begin
    DBAliasName1:=AtmHComboBox1.Items[AtmHComboBox1.Itemindex] ;
    IniFile := TIniFile.Create(ExtractFilePath(Application.ExeName)+'\AtmAzmn.Ini');
    IniFile.WriteString ('Carcom', 'AliasName' ,DBAliasName1);
    IniFile.Free;

end;

end.
